# Unmaintained!

I don't maintain nor update this repository anymore.

# Startpage hugo theme
## Minimalistic, nice looking and straight forward theme. Includes single pages and a blogging section.

![Screenshot of startpage theme](images/screenshot.png)

Startpage is a very basic theme for hugo with following features:

* Modern and minimal look and feel
* Responsiveness
* Single pages
* Article pages / blogging section
* RSS/Atom-Feed for articles
* Navigation menu at top
* Social icons at bottom (Mastodon, Jabber and so on...)
* Legal links at the very bottom

## Installation

Installation is as simple as with every other hugo theme. Just run following commands inside the `themes`-folder of your hugo site:

`mkdir startpage`

`git clone https://codeberg.org/jeybe/startpage-theme_hugo.git ./startpage`

That's all! Now let's get your site running...

## Configuration

It requires a few more steps to get your site running. Take a look inside the [`exampleSite`](exampleSite/) folder of this theme. You'll find a file called [`config.toml`](exampleSite/config.toml). To use it, copy it to the root directory of your Hugo page and customize it to your needs.

It's quite self-explanatory, yet something needs to be noted:

### Social icons

The `menu.social` paramters are to define the litte row of icons at the bottom of your page. They can be used to link to your social accounts or any other site, displayed with proper icons. **But** the `name` paramter has to be available at [forkawesome](https://forkaweso.me/Fork-Awesome/icons/) as it will define which icon will be displayed. Make sure to **spell it correctly**, as otherwise **no icon will be shown!**

### Post types

One more important thing: There are **three types of posts**, which must be specified at the top of each post with `type: "page"`, `type: "article"` or `type: "custom"`. Posts of the type `page` are single page entrys that won't be listet at the blogging section or in the RSS/Atom-Feed. Posts of the type `article` are meant to be blog posts and, thus, will be listet at the blogging sections and in the RSS/Atom-Feed. Post of the type `custom` are to be used with html and **not markdown** files, so that you can structure everything yourself (won't be listet at the blogging section and the feed either). As this theme uses the w3.css framework this provides you with very much flexibility if you want to give some pages special layouts. Have a look at [Mentions - W3.CSS](#Mentions) for more information.

## Mentions

This theme is based upon:

### W3.CSS

A lot of inspiration is taken from the w3.css framework. Be it whole files, some example html structure, custom layout or javascript snippets. Take a look:

* [W3.CSS](https://www.w3schools.com/w3css/default.asp)
* [W3.CSS Start Page Template](https://www.w3schools.com/w3css/w3css_templates.asp) - [Demo](https://www.w3schools.com/w3css/tryw3css_templates_start_page.htm) - [Try it](https://www.w3schools.com/w3css/tryit.asp?filename=tryw3css_templates_start_page&stacked=h)

## Licence

All files in this repository are licensed under the [MIT license](LICENCE). Excepted from this are fonts, including symbol fonts, which are licensed under the [SIL Open Fonts License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL&_sc=1).
